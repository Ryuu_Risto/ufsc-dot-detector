A simple program for analysing dot patterns in a series of images.
Built as a contribution to the [Unfavorable Semicircle](https://www.unfavorablesemicircle.com) project.
