const { table } = require("console");
const fs = require("fs");
const jimp = require("jimp");

let dir = fs.readdirSync("frame");

let array = Array.from(Array(50), () => Array(50).fill(""));

let D2H = c => {
    return '#' + ("000000" + (((c & 0xFF) << 16) + (c & 0xFF00) + ((c >> 16) & 0xFF)).toString(16)).slice(-6);
}

async function analyze(path) {
	let id = path.slice(0, -4);
	console.log(`[#${id}] Queueing frame`);
	let image;
	try {
		image = await jimp.read(`./frame/${path}`);
	} catch (err) {throw err};

	// determine background color
	let scan = [];
	image.scan(4, 4, 8, 8, (x, y, idx) =>{
		let c = image.bitmap.data[idx] + (image.bitmap.data[idx+1] << 8) + (image.bitmap.data[idx+2] << 16);
		scan.push(c);
	})
	let count = new Map();
	for (let px of scan) {
		if (count.has(px)) count.set(px, count.get(px) + 1);
		else count.set(px, 1);
	}
	let bg, amt = 0;
	for (let c of count.entries()) {
		if (c[1] > amt) {
			bg = c[0];
			amt = c[1];
		}
	}
	console.log(`[#${id}] Background determined as ${D2H(bg)}`);
	if (bg == 0) return;

	// find the "dot"
	let baseline = (Math.max(	bg & 0xFF,
								(bg & 0xFF00) >> 8,
								(bg & 0xFF0000) >> 16)
				+ 	Math.min(	bg & 0xFF,
								(bg & 0xFF00) >> 8,
								(bg & 0xFF0000) >> 16)
					)/2;
	let dot = [null, null];
	for (let d of image.scanIterator(0,0,image.bitmap.width,image.bitmap.height)) {
		let luma = (Math.max(	d.image.bitmap.data[d.idx],
								d.image.bitmap.data[d.idx+1],
								d.image.bitmap.data[d.idx+2])
				+ 	Math.min(	d.image.bitmap.data[d.idx],
								d.image.bitmap.data[d.idx+1],
								d.image.bitmap.data[d.idx+2])
				)/2;
		if (baseline - luma > 8.5) {
			array[d.x][d.y]++;
			console.log(`[#${id}] Dot found at [${d.x},${d.y}]`);
			break;
		}
	}
}


for (let frame of dir) {
	analyze(frame);
}

setTimeout(() => {
	let tmp = "  ";
	for (let i = 0; i < 50; i++) tmp += i%10 + " ";
	console.log(tmp);
	for (let i = 0; i < 50; i++) {
		tmp = i%10 + " ";
		for (let j = 0; j < 50; j++) {
			tmp += array[i][j] > 0 ? "██" : "  ";
		}
		console.log(tmp);
	}

}, 1000);
